INSTALL_PREFIX = $$(QCM_INSTALL)
isEmpty(INSTALL_PREFIX) {
    INSTALL_PREFIX = /usr
}

TEMPLATE = lib
CONFIG += plugin
TARGET = $$qtLibraryTarget(Receiver)
INCLUDEPATH += $$INSTALL_PREFIX/include/
LIBS += -L$$INSTALL_PREFIX/lib/ -lqcm-runtime

HEADERS += receiver.h
SOURCES += receiver.cpp

target.path = $$INSTALL_PREFIX/lib/
INSTALLS += target
