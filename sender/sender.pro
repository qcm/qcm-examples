INSTALL_PREFIX = $$(QCM_INSTALL)
isEmpty(INSTALL_PREFIX) {
    INSTALL_PREFIX = /usr
}

TEMPLATE = lib
CONFIG += plugin
TARGET = $$qtLibraryTarget(Sender)
INCLUDEPATH += $$INSTALL_PREFIX/include/
LIBS += -L$$INSTALL_PREFIX/lib/ -lqcm-runtime

HEADERS += sender.h
SOURCES += sender.cpp

target.path = $$INSTALL_PREFIX/lib/
INSTALLS += target
